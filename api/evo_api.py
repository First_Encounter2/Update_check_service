from typing import Any

import requests

SESSION = requests.Session()


def select_publisher_apps(publisher_id: str) -> list[dict[str, Any]]:
    """Get a list of dict for all applications by publisher key"""
    URL = f'https://market.evotor.ru/api/v1/catalog/public/publishers/{publisher_id}/apps'

    request = SESSION.get(URL)
    return request.json()


def select_an_app(app_id: str) -> dict[str, Any]:
    """Get application properties dictionary by unique id"""
    URL = f'https://market.evotor.ru/api/v1/catalog/public/products/{app_id}'

    request = SESSION.get(URL)
    return request.json()
