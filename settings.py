import os

from enum import IntEnum

from dotenv import load_dotenv
from pydantic import BaseSettings, BaseModel
from pydantic.env_settings import SettingsSourceCallable
from yaml import safe_load

load_dotenv()


def yaml_parser(settings: BaseSettings) -> dict:
    """parse yaml file and return dict with settings"""
    config_path = os.environ.get("CONFIG__CONFIG_PATH")
    if not config_path:
        config_path = "../config/config.yaml"
    with open(config_path, "r", encoding=settings.__config__.env_file_encoding) as f:
        return safe_load(f)


class TimeUnit(IntEnum):
    """define valid time unit constants and the number of seconds for each constant"""
    SEC = 1
    MIN = 60
    HOUR = 3600
    DAY = 87600

    @classmethod
    def __get_validators__(cls):
        """create a dictionary where the key is the name of the constant,
        and the value of the key is the value of the constant"""
        cls.dict_of_allowed_values = {v: k for v, k in cls.__members__.items()}
        yield cls.validate

    @classmethod
    def validate(cls, v):
        """we check by key whether there is such a value, if true - return the value of the key"""
        try:
            return cls.dict_of_allowed_values[v.upper()]
        except KeyError:
            raise ValueError('invalid value')


class Monitor(BaseModel):
    """type checking for Monitor section settings"""

    publisher_id: str


class Telegram(BaseModel):
    """type checking for Telegram section settings"""

    bot_token: str
    chat_id: int


class Db(BaseModel):
    """type checking for Db section settings"""

    path_to_file: str


class Period(BaseModel):
    """type checking for Period section settings"""

    every: int
    time_unit: TimeUnit

    def to_secs(self) -> int:
        """return the number of seconds given the specified time unit and the delay in that time unit"""
        return self.every * self.time_unit


class Settings(BaseSettings):
    """type checking for Period section settings"""

    monitor: Monitor
    telegram: Telegram
    db: Db
    period: Period

    class Config:
        """class for connecting .env files, defining additional configuration sources"""

        env_file = ".env"
        env_file_encoding = "utf-8"
        env_nested_delimiter = "__"

        @classmethod
        def customise_sources(
                cls,
                init_settings: SettingsSourceCallable,
                env_settings: SettingsSourceCallable,
                file_secret_settings: SettingsSourceCallable,
        ):
            return env_settings, yaml_parser, init_settings, file_secret_settings


options = Settings()
