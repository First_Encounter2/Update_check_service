from datetime import datetime
import time
from settings import options


def display_the_current_time_with_delay() -> None:
    """executing the command to display the current time"""
    delay_in_sec = options.period.to_secs()
    while True:
        print(f"Начало: {datetime.now()}")
        time.sleep(delay_in_sec)
        print(f"Конец:  {datetime.now()}")


def main():
    """main func"""
    try:
        display_the_current_time_with_delay()
    except KeyboardInterrupt:
        print('Остановлено пользователем')


if __name__ == '__main__':
    main()
