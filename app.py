from settings import options


# all_apps_with_id_url: "https://market.evotor.ru/api/v1/catalog/public/publishers/{publisher_id}/apps"
# base_apps_url: "https://market.evotor.ru/api/v1/catalog/public/products/{app_id}}"
# telegram_api_url: "https://api.telegram.org/bot:{token}/{method}"


def main():
    """application launch"""
    print(options)


if __name__ == "__main__":
    main()
