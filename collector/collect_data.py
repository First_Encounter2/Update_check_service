import json

from api import evo_api
from settings import options


def pick_up_app_id(pub_key: str) -> list:
    """return a list of IDs"""
    return [i.get('appId') for i in evo_api.select_publisher_apps(pub_key)]


def collect_app_info() -> None:
    """return a list of required application keys"""
    with open('new.json', 'w', encoding='utf-8') as f:
        json.dump([evo_api.select_an_app(i) for i in pick_up_app_id(options.monitor.publisher_id)],
                  f, ensure_ascii=False, indent=4)


def create_a_dict_from_the_resulting_list_of_values() -> list:
    """return a filtered list of dictionaries"""
    final_list = []
    essential_keys = ['shortName', 'versionString', 'versionSummary', 'lastModified']
    with open('new.json', 'r', encoding='utf-8') as f:
        entire_json = json.load(f)

    for i in entire_json:
        new_dict = {key: val for key, val in i.items() if key in essential_keys}
        final_list.append(new_dict)

    return final_list


if __name__ == "__main__":
    print(create_a_dict_from_the_resulting_list_of_values())
